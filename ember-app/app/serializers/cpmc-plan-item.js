import { Serializer as PlanItemSerializer } from
  '../mixins/regenerated/serializers/cpmc-plan-item';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(PlanItemSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
