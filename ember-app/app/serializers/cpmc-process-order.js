import { Serializer as ProcessOrderSerializer } from
  '../mixins/regenerated/serializers/cpmc-process-order';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(ProcessOrderSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
