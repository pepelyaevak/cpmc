import { OfflineSerializer as DSERoutSerializer } from
  '../mixins/regenerated/serializers/cpmc-d-s-e-rout-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(DSERoutSerializer, {
});
