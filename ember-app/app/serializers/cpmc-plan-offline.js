import { OfflineSerializer as PlanSerializer } from
  '../mixins/regenerated/serializers/cpmc-plan-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(PlanSerializer, {
});
