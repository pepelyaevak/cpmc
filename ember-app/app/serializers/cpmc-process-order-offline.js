import { OfflineSerializer as ProcessOrderSerializer } from
  '../mixins/regenerated/serializers/cpmc-process-order-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(ProcessOrderSerializer, {
});
