import { OfflineSerializer as DSESerializer } from
  '../mixins/regenerated/serializers/cpmc-d-s-e-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(DSESerializer, {
});
