import { OfflineSerializer as MaterialSerializer } from
  '../mixins/regenerated/serializers/cpmc-material-offline';
import ResourseSerializer from './cpmc-resourse-offline';

export default ResourseSerializer.extend(MaterialSerializer, {
});
