import { OfflineSerializer as MachineSerializer } from
  '../mixins/regenerated/serializers/cpmc-machine-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(MachineSerializer, {
});
