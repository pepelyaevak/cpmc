import { Serializer as WorkTypeSerializer } from
  '../mixins/regenerated/serializers/cpmc-work-type';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(WorkTypeSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
