import { Serializer as ToolSerializer } from
  '../mixins/regenerated/serializers/cpmc-tool';
import ResourseSerializer from './cpmc-resourse';

export default ResourseSerializer.extend(ToolSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
