import { OfflineSerializer as OperationSerializer } from
  '../mixins/regenerated/serializers/cpmc-operation-offline';
import ResourseSerializer from './cpmc-resourse-offline';

export default ResourseSerializer.extend(OperationSerializer, {
});
