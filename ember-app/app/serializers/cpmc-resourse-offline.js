import { OfflineSerializer as ResourseSerializer } from
  '../mixins/regenerated/serializers/cpmc-resourse-offline';
import __ApplicationSerializer from './application-offline';

export default __ApplicationSerializer.extend(ResourseSerializer, {
});
