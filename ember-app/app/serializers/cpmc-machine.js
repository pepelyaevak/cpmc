import { Serializer as MachineSerializer } from
  '../mixins/regenerated/serializers/cpmc-machine';
import __ApplicationSerializer from './application';

export default __ApplicationSerializer.extend(MachineSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
