import { Serializer as OperationSerializer } from
  '../mixins/regenerated/serializers/cpmc-operation';
import ResourseSerializer from './cpmc-resourse';

export default ResourseSerializer.extend(OperationSerializer, {
  /**
  * Field name where object identifier is kept.
  */
  primaryKey: '__PrimaryKey'
});
